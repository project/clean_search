# Clean Search

Clean Search module adds a way to clean up form values 
from search fields after form submission and form initialization.
You can configure the module to toggle functionality, or configure 
custom search form values to be cleaned.


## Installation:

  * Installation is like all normal drupal modules:
   * Extract the 'clean_search' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).
   * Go to /admin/modules and enable the 'clean_search' module

## Dependencies:

  * The Clean Search module has no dependencies to other modules.

## Conflicts/known issues:

  * There are no current conflicts nor known issues with this module.
  
## Configuration

  * Enable or disable the functionality with a toggle.
  * Specify custom field names to be cleaned up.
  * Specify custom form names which need to be cleaned up.
