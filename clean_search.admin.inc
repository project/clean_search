<?php

/**
 * @file
 * Clean Search configuration.
 */

/**
 * Custom form for module configuration.
 */
function clean_search_admin_form($form, &$form_state) {

  // Honeypot Configuration.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['clean_search__enable_functionality'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable functionality'),
    '#description' => t('Activate the field cleanup logic for search forms.<br\>By default it applies to the search term only, and not advanced or custom filters.'),
    '#default_value' => variable_get('clean_search__enable_functionality', 1),
  );
  $form['general']['clean_search__form_field_names'] = array(
    '#type' => 'textarea',
    '#title' => t('Field names for cleanup'),
    '#description' => t('Enter a list of field names (separated by newlines) which should be considered for cleanup.<br/>By entering something here, you override the default field names which are cleanup: search_block_form and keys.<br/>Remember that only the following forms are cleaned by default: search-form and search-block-form.'),
    '#default_value' => variable_get('clean_search__form_field_names', ''),
  );
  $form['general']['clean_search__form_names'] = array(
    '#type' => 'textarea',
    '#title' => t('Form names for cleanup'),
    '#description' => t('Enter a list of form names (separated by newlines) which should be considered for cleanup.<br/>By entering something here, you override the default form names which are cleanup: search-form and search-block-form.'),
    '#default_value' => variable_get('clean_search__form_names', ''),
  );
  return system_settings_form($form);
}
